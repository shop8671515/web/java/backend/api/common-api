include common.mk
.ONESHELL:
SHELL := bash
.SHELLFLAGS = -euo pipefail -c
ARTIFACTORY_DEPLOY_URL := https://gitlab.com/api/v4/projects/51024674/packages/maven

dictionary-java: dictionary-spring dictionary-feign

dictionary-spring:
	$(MAKE) docker-build VARIANT=spring MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/src/common.yml'
	$(MAKE) install-java VARIANT=spring MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/src/common.yml'

dictionary-feign:
	$(MAKE) docker-build VARIANT=feign MODULE=dictionary EXTRA_DEPS="$(SHARED_DIR)/src/common.yml"
	$(MAKE) install-java VARIANT=feign MODULE=dictionary EXTRA_DEPS="$(SHARED_DIR)/src/common.yml"

dictionary-lint:
	$(MAKE) docker-lint MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/src/common.yml'

dictionary-ts:
	$(MAKE) docker-build VARIANT=typescript MODULE=dictionary EXTRA_DEPS='$(SHARED_DIR)/src/common.yml'


