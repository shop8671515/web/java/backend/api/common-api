.ONESHELL:
SHELL := bash
.SHELLFLAGS = -euo pipefail -c
POSTFIX := .openapi.yml
OPENAPI_MAKEFILE := /opt/makefile/Makefile
OPENAPI_DIR := .
OPENAPI_CASING := camel
OPENAPI_PARAMS :=
SHARED_DIR = $(OPENAPI_DIR)
FILES = $(wildcard $(OPENAPI_DIR)/*$(POSTFIX))
VERSIONS = $(FILES:$(OPENAPI_DIR)/%$(POSTFIX)=$(OPENAPI_DIR)/%.VERSION)
DOCKER_IMAGE_VERSION := test
DOCKER_IMAGE := bananawhite98/openapi-generator:$(DOCKER_IMAGE_VERSION)
GROUP := ryszardszewczyk2204
SPECTRAL_RULESET = $(SHARED_DIR)/rszewczyk.js
ARTIFACTORY_DEPLOY_URL :=
IBM_LINT_PARAMS :=

# pomocnicze reguły
## drukuje numery wersji
versions:
	@make docker-run MAKE_ARGS=versions

## drukuje zależności dla npm
libs-npm: $(VERSIONS)
	@make docker-run MAKE_ARGS=libs-npm

## drukuje zależności dla mvn (spring-api)
libs-mvn-spring:
	@make docker-run MAKE_ARGS=".deps-mvn ARTIFACT=api GROUP=$(GROUP)"

## drukuje zależności dla mvn (feign-api)
libs-mvn-feign:
	@make docker-run MAKE_ARGS=".deps-mvn ARTIFACT=feign GROUP=$(GROUP)"

## czyści pliki tymczasowe
clean:
	@make docker-run MAKE_ARGS="clean"

## drukuje reguły budowania make, które należy dodać do tego pliku po dodaniu nowej specyfikacji
rules-make:
	@if [[ -z "$(MODULE)" ]]; then
		@printf "BŁĄD: Brakujący parametr MODULE\n"
		@printf "make $@ MODULE=<nazwa modułu>\n"
		exit 1
	fi
	@echo "Reguły do dodania do Makefile"
	@echo "$(MODULE)-java: $(MODULE)-spring $(MODULE)-feign"
	@echo
	@echo "$(MODULE)-spring:"
	@echo -e "\tmake docker-build VARIANT=spring MODULE=$(MODULE)"
	@echo -e "\tmake install-java VARIANT=spring MODULE=$(MODULE)"
	@echo
	@echo "$(MODULE)-feign:"
	@echo -e "\tmake docker-build VARIANT=feign MODULE=$(MODULE)"
	@echo -e "\tmake install-java VARIANT=feign MODULE=$(MODULE)"
	@echo
	@echo "$(MODULE)-lint:"
	@echo -e "\t@make docker-lint MODULE=$(MODULE)"
	@echo
	@echo "$(MODULE)-ts:"
	@echo -e "\tmake docker-build VARIANT=typescript MODULE=$(MODULE)"

## drukuje reguły budowania w ramach CI, które należy dodać do tego .gitlab_ci.yml
## reguły powinny być dodawane w kolejności alfabetycznej
rules-ci:
	@if [[ -z "$(MODULE)" ]]; then
		@printf "BŁĄD: Brakujący parametr MODULE\n"
		@printf "make $@ MODULE=<nazwa modułu>\n"
		exit 1
	fi
	@echo "Reguły do dodania do .gitlab_ci.yml"
	@echo "$(MODULE)_api_feign:"
	@echo "  extends: .api_java"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"
	@echo
	@echo "$(MODULE)_api_spring:"
	@echo "  extends: .api_java"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"
	@echo
	@echo "$(MODULE)_api_ts:"
	@echo "  extends: .api_ts"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"
	@echo
	@echo "$(MODULE)_lint:"
	@echo "  extends: .lint_openapi"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"


## inicjalizuje konfigurację dla nowej specyfikacji
config-init: config/$(MODULE)-feign.yml config/$(MODULE)-spring.yml config/$(MODULE)-typescript.yml
	@set -e
	@if [[ -z "$(MODULE)" ]]; then
		@printf "BŁĄD: Brakujący parametr MODULE\n"
		@printf "make $@ MODULE=<nazwa modułu>\n"
		exit 1
	fi
	@echo "Skopiowano konfigurację. Pliki należy wyedytować."
	@ls config/$(MODULE)*.yml
	@echo
	@make --no-print-directory rules-make MODULE=$(MODULE)
	@echo
	@make --no-print-directory rules-ci MODULE=$(MODULE)

## uruchamia powłokę obrazu z zamontowanymi źródłami
shell:
	docker run \
		-e "CI_PROJECT_DIR=/src" \
		-u "$$(id -u):$$(id -g)" \
		-v $(PWD):/src \
		-e HOME=$$HOME \
		-v ~/.m2:$$HOME/.m2 \
		-v ~/.cache:/$$HOME/.cache \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--rm -it \
		--workdir /src \
		$(DOCKER_IMAGE) bash

## pobiera najnowszą wersję obrazu z narzędziami
docker-pull:
	docker pull $(DOCKER_IMAGE)

## wypisuje informacje przydatne przy rozwiązywaniu problemów
diagnose:
	@echo "Aktualna rewizja:"
	git log -1
	@echo -e '\nWersje API:'
	$(MAKE) --silent versions
	@echo -e '\nWersja make:'
	make --version
	@echo -e '\nWersja bash:'
	bash --version
	@echo -e '\nWersja dockera:'
	docker --version
	@echo -e '\nWersja obrazu (make docker-pull pozwala na pobranie ostatniej wersji obrazu):'
	@docker images | awk '/openapi\/tools/ {if ($$2 == $(DOCKER_IMAGE_VERSION)){print}}'
	@echo -e "\nWersja javy na obrazie"
	docker run --rm $(DOCKER_IMAGE) java --version
	@echo -e "\nWersja openapi-generatora na obrazie"
	docker run --rm $(DOCKER_IMAGE) openapi-generator --version
	@echo -e "\nWersja lintera ibm-openapi-validator na obrazie"
	docker run --rm $(DOCKER_IMAGE) lint-openapi --version


lint-all:
	@for f_path in $(OPENAPI_DIR)/src/*$(POSTFIX); do
		filename=$$(basename "$${f_path}")
		stem="$${filename%%$(POSTFIX)}"
		$(MAKE) --silent "$${stem}-lint"
	done

java-all:
	@for f_path in $(OPENAPI_DIR)/src/*$(POSTFIX); do
		filename=$$(basename "$${f_path}")
		stem="$${filename%%$(POSTFIX)}"
		$(MAKE) --silent "$${stem}-java"
	done

ts-all:
	@for f_path in $(OPENAPI_DIR)/src/*$(POSTFIX); do
		filename=$$(basename "$${f_path}")
		stem="$${filename%%$(POSTFIX)}"
		printf "Budowanie $${stem}\n"
		$(MAKE) --silent "$${stem}-ts"
	done

# zaawansowane reguły
install-java:
	@if [[ -z "$(MODULE)" ]] || [[ -z "$(VARIANT)" ]]; then
		@printf "BŁĄD: Brakujący parametr MODULE albo VARIANT\n"
		@printf "make $@ MODULE=<nazwa modułu> VARIANT=<nazwa wariantu>\n"
		exit 1
	fi
	MVN_OPTS="-Duser.home=$${HOME}"; \
	docker run \
		-e "CI_PROJECT_DIR=/src" \
		-e HOME=$$HOME \
		-u "$$(id -u):$$(id -g)" \
		-v $(PWD):/src \
		-v ~/.m2:$$HOME/.m2 \
		-v ~/.cache:/$$HOME/.cache \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c "cd /src && make -f $(OPENAPI_MAKEFILE) \
			OPENAPI_DIR=/src/$(OPENAPI_DIR) \
			FILES_POSTFIX=$(POSTFIX) \
			FILES=/src/$(OPENAPI_DIR)/src/$(MODULE)$(POSTFIX) \
			MVN_OPTS=\"$${MVN_OPTS}\" \
			EXTRA_DEPS='$(EXTRA_DEPS)' \
			install-$(VARIANT)"

docker-run:
	@docker run \
		-e "CI_PROJECT_DIR=/src" \
		-u "$$(id -u):$$(id -g)" \
		-v $(PWD):/src \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c 'cd /src \
			&& make -f $(OPENAPI_MAKEFILE) \
			OPENAPI_DIR=/src/$(OPENAPI_DIR) \
			FILES_POSTFIX=$(POSTFIX) \
			$(MAKE_ARGS)'

docker-build:
	task=java
	if [[ $(VARIANT) == "typescript" ]]; then
	    task=ts
	fi
	docker run \
		-e "CI_PROJECT_DIR=/src" \
		-e HOME=$$HOME \
		-u "$$(id -u):$$(id -g)" \
		-v "$(PWD)":/src \
		-v ~/.m2:$$HOME/.m2 \
		-v ~/.cache:/$$HOME/.cache \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c "cd /src && make ci-gen-$${task} VARIANT=$(VARIANT) MODULE=$(MODULE) ARTIFACTORY_DEPLOY_URL=$(ARTIFACTORY_DEPLOY_URL) EXTRA_DEPS='$(EXTRA_DEPS)'"

docker-lint:
	@docker run \
		-e "CI_PROJECT_DIR=/src" \
		-u "$$(id -u):$$(id -g)" \
		-v $(PWD):/src \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c 'cd /src && \
		   make -f $(OPENAPI_MAKEFILE) \
			OPENAPI_DIR=/src/$(OPENAPI_DIR) \
			FILES_POSTFIX=$(POSTFIX) \
			FILES=/src/$(OPENAPI_DIR)/src/${MODULE}$(POSTFIX) \
			SPECTRAL_RULESET=$(SPECTRAL_RULESET) \
			IBM_LINT_PARAMS=$(IBM_LINT_PARAMS) \
			OPENAPI_CASING=$(OPENAPI_CASING) \
			EXTRA_DEPS='$(EXTRA_DEPS)' \
			lint'

ci-gen-java:
	SPEC="$(OPENAPI_DIR)/src/$(MODULE)$(POSTFIX)"
	MVN_OPTS="-s $${CI_PROJECT_DIR}/.mvn/settings.xml"; \
	if [[ ! -v CI ]]; then
		MVN_OPTS="-Duser.home=$${HOME}"; \
	fi
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		OPENAPI_PARAMS="$(OPENAPI_PARAMS)" \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		MVN_OPTS="$${MVN_OPTS}" \
		ARTIFACTORY_DEPLOY_URL=$(ARTIFACTORY_DEPLOY_URL) \
		$(VARIANT)-api

ci-lint:
	SPEC="$(OPENAPI_DIR)/src/$(MODULE)$(POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		SPECTRAL_RULESET=$(SPECTRAL_RULESET) \
		OPENAPI_CASING=$(OPENAPI_CASING) \
		lint

ci-push-java:
	SPEC="$(OPENAPI_DIR)/src/$(MODULE)$(POSTFIX)"
	MVN_OPTS="-s $${CI_PROJECT_DIR}/.mvn/settings.xml"; \
	if [[ ! -v CI ]]; then
		MVN_OPTS="-Duser.home=$${HOME}"; \
	fi
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		MVN_OPTS="$${MVN_OPTS}" \
		push-$(VARIANT)

ci-gen-ts:
	SPEC="$(OPENAPI_DIR)/src/$${MODULE}$(POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		OPENAPI_PARAMS="$(OPENAPI_PARAMS)" \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		typescript-api

ci-push-ts:
	echo "@$(GROUP):registry=https://registry.npmjs.org/" >| /root/.npmrc
	echo "//registry.npmjs.org/:_authToken=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" >> /root/.npmrc
	SPEC="$(OPENAPI_DIR)/src/$${MODULE}$(POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		typescript-api push-typescript

config/%.yml:
	@set -e
	@if [[ -z "$(MODULE)" ]]; then
		@printf "BŁĄD: Brakujący parametr MODULE\n"
		exit 1
	fi
	filename="$$(basename $@)"
	@SRC="config/file-$$(echo $$filename | rev | cut -d"-" -f1 | rev)"
	cp $$SRC $@

## reguła do budowania API z użyciem lokalnej kopii
## https://gitlab.enigma.com.pl/enigma/build-tools/images/openapitools
## zamiast obrazu dockerowego do testowania nowych rozwiązać i naprawiania
## błędów
## przykładowe użycie:
## make debug-build OPENAPITOOLS_DIR=<ścieżka> MODULE=file VARIANT=feign
debug-build:
	make -f $(OPENAPITOOLS_DIR)/opt/makefile/Makefile ARTIFACTORY_DEPLOY_URL=$(ARTIFACTORY_DEPLOY_URL) OPENAPI_DIR=$(OPENAPI_DIR) FILES_POSTFIX=$(POSTFIX) FILES="$(OPENAPI_DIR)/src/$(MODULE)$(POSTFIX)" OPENAPI_PARAMS="$(OPENAPI_PARAMS)" VERSION="debug" $(VARIANT)-api
